package com.bsuir.graph.algorithm;

public class PrimMSTFinder implements MSTFinder {

    private int readMatrixCount;

    @Override
    public int[][] findMST(int[][] graph) {
        readMatrixCount = 0;

        final int[][] mstGraph = new int[graph.length][graph.length];
        final boolean[] includedVertexes = new boolean[graph.length];
        includedVertexes[0] = true;
        while (!isAllVertexesIncluded(includedVertexes)) {
            int vertexPosition = -1;
            int minWeight = Integer.MAX_VALUE;
            int minWeightPosition = -1;
            for (int i = 0; i < includedVertexes.length; i++) {
                if (includedVertexes[i]) {
                    for (int j = 0; j < graph.length; j++) {
                        if (graph[j][i] != 0 && !includedVertexes[j]) {
                            readMatrixCount++;
                            if (minWeight > graph[i][j]) {
                                vertexPosition = i;
                                minWeight = graph[i][j];
                                minWeightPosition = j;
                            }
                        }
                    }
                }
            }
            includedVertexes[minWeightPosition] = true;
            mstGraph[vertexPosition][minWeightPosition] = minWeight;
            mstGraph[minWeightPosition][vertexPosition] = minWeight;
        }

        return mstGraph;
    }

    @Override
    public int getReadMatrixCount() {
        return readMatrixCount;
    }


    private boolean isAllVertexesIncluded(boolean[] includedVertexes) {
        for (boolean includedVertex : includedVertexes) {
            if (!includedVertex) {
                return false;
            }
        }
        return true;
    }
}
