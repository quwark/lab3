package com.bsuir.graph.algorithm;

public class KruskalMSTFinder implements MSTFinder {

    private int readMatrixCount;

    @Override
    public int[][] findMST(int[][] graph) {
        readMatrixCount = 0;
        final int[][] mstGraph = new int[graph.length][graph.length];
        final int[] parent = new int[graph.length];
        int n = 1;
        int a = 0;
        int b = 0;
        int u = 0;
        int v = 0;
        int min;
        while (n < graph.length) {
            min = Integer.MAX_VALUE;
            for (int i = 0; i < graph.length; i++) {
                for (int j = 0; j < graph.length; j++) {
                    if (graph[i][j] != 0) {
                        readMatrixCount++;
                        if (graph[i][j] < min) {
                            min = graph[i][j];
                            a = i;
                            u = i;
                            b = j;
                            v = j;
                        }
                    }
                }
            }
            u = find(u, parent);
            v = find(v, parent);
            if (union(u, v, parent)) {
                mstGraph[a][b] = min;
                mstGraph[b][a] = min;
                n++;
            }
            graph[a][b] = 0;
            graph[b][a] = 0;
        }
        readMatrixCount /= 2;
        return mstGraph;
    }

    @Override
    public int getReadMatrixCount() {
        return readMatrixCount;
    }

    private int find(int i, int[] parent) {
        while (parent[i] != 0) {
            i = parent[i];
        }
        return i;
    }
    private boolean union(int i, int j, int[] parent) {
        if (i != j) {
            parent[j] = i;
            return true;
        }
        return false;
    }

}
