package com.bsuir.graph.algorithm;

public interface MSTFinder {

    int[][] findMST(int[][] graph);

    int getReadMatrixCount();
}
