package com.bsuir.graph;

import com.bsuir.graph.algorithm.KruskalMSTFinder;
import com.bsuir.graph.algorithm.MSTFinder;
import com.bsuir.graph.algorithm.PrimMSTFinder;

public class Runner {

    public static void main(String[] args) {
        int[][] graph = generateGraph(20);

        System.out.println("Graph:");
        displayGraph(graph);

        MSTFinder primAlgorithm = new PrimMSTFinder();
        int[][] primMSTGraph = primAlgorithm.findMST(graph);
        System.out.println("Prim MST Graph (read matrix count = " + primAlgorithm.getReadMatrixCount() + ") :");
        displayGraph(primMSTGraph);


        MSTFinder kruskalAlgorithm = new KruskalMSTFinder();
        int[][] kruskalMSTGraph = kruskalAlgorithm.findMST(graph);
        System.out.println("Kruskal MST Graph (read matrix count = " + kruskalAlgorithm.getReadMatrixCount() + ") :");
        displayGraph(kruskalMSTGraph);
    }

    private static int[][] generateGraph(int vertexNumber) {
        int[][] graph = new int[vertexNumber][vertexNumber];
        for (int i = 0; i < vertexNumber - 1; i++) {
            for(int j = i + 1; j < vertexNumber; j++) {
                graph[i][j] = getWeight();
                graph[j][i] = graph[i][j];
            }
        }
        return graph;
    }

    private static int getWeight() {
        return (int) ((Math.random() * 9) + 1);
    }

    private static void displayGraph(int[][] graph) {
        for (int i = 0; i < graph.length; i++) {
            for(int j = 0; j < graph[i].length; j++) {
                System.out.print(String.format("%4d", graph[i][j]));
            }
            System.out.println();
        }
    }
}
